// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBnT1SbG0_9kjyDkkFaOC3hLcy8741B8PM",
    authDomain: "project-game-25410.firebaseapp.com",
    databaseURL: "https://project-game-25410.firebaseio.com",
    projectId: "project-game-25410",
    storageBucket: "project-game-25410.appspot.com",
    messagingSenderId: "1011968563042",
    appId: "1:1011968563042:web:8b3e81a42cf0e0b414fe3b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SideNavService } from './services/app/side-nav.service';
import { MatSidenav } from '@angular/material/sidenav';
import { LiveGameService } from './services/game/live-game.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'Project Game';

  constructor(private _router:Router, public sideNavService: SideNavService, private liveGameService: LiveGameService) {}

  navToPage(page:string) {
    this._router.navigate(['home'])
  }

  isNavOpen() {
    return this.sideNavService.navOpen;
  }

  async toggleNav(sidebar:MatSidenav) {
    await sidebar.toggle();
    this.liveGameService.engineResizeViewPort();
  }
}

export interface User {
    id: string;
    email: string;
    firstName?: string;
    lastName?: string;
    region?: string;
    description?: string;
    type: string;
}
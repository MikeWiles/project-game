import { Key } from 'ts-keycode-enum';

export interface KeyControl {
    value:number,
    keys: Key[]
}
import { Character } from './Character';

export interface Game {
    _id?: string;
    isOfflineGame?: boolean;
    characters: Character[];
}
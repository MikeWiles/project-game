import * as BABYLON from "babylonjs";

export interface Character {
    _id?: string;
    name: string;
    colour: string;
    gameplay:GamePlay;
    mainMesh?: BABYLON.Mesh;
    textMesh?: BABYLON.Mesh;
    trackingMesh?: BABYLON.Mesh;
    animation: {
        skeleton?: BABYLON.Skeleton;
        animatables: {[key: string]:BABYLON.Animatable}
    };
}

export interface GamePlay {
    _id?:string;
    x: number;
    y: number;
    z: number;
    rotationY: number;
    speed: number;
    acceleration: number;
    state: 'rest' | 'run' | 'walk' | 'dance'
}
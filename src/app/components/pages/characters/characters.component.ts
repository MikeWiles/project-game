import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/data/user-data.service';
import { User } from 'src/app/models/User';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.less'],
  animations: [
    trigger('openClosed', [
      state('open', style({
        overflow: 'hidden',
        height: '*',
        opacity: '1',
        padding: '20px',
        margin: '10px 0 10px 0'
      })),
      state('closed', style({
        overflow: 'hidden',
        height: '0',
        opacity: '0',
        padding: '0',
        margin: '0'
      })),
      transition('open <=> closed', animate('400ms ease-in-out')),
    ])
  ]
})
export class CharactersComponent implements OnInit {

  public hasAddedCharacter = false;
  public newCharacter:User;
  public allUsers: any[];

  characterForm:FormGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl(''),
    region: new FormControl(''),
    description: new FormControl(''),
  });

  constructor(public router:Router, public userService:UserService) { }

  async ngOnInit() {
    await this.getUsers();
  }

  public togglehasAddedCharacter() {
    this.hasAddedCharacter = !this.hasAddedCharacter;
  }

  async getUsers(){
    this.userService.getUsers()
    .subscribe(result => {
      this.allUsers = result.map(actions => {
        const id = actions.payload.doc.id;
        const data = actions.payload.doc.data();
        return {id,...(<any>data)};
      });
    });
  }

  public deleteCharacter(character){
    this.userService.deleteUser(character.id);
  }

  public async onSubmit(user:User) {
    this.characterForm.reset();
    this.hasAddedCharacter = false;
    await this.userService.createUser(user)
  }

  public get hasLoaded() {
    return this.allUsers && this.allUsers.length>0;
  }
}

import { Component, OnInit, ViewChild, OnDestroy, HostListener } from '@angular/core';
import { LiveGameService } from 'src/app/services/game/live-game.service';
import { Game } from 'src/app/models/Game';
import { Character } from 'src/app/models/Character';
import { GameDataService } from 'src/app/services/data/game-data.service';
import { ControlsService } from 'src/app/services/game/controls.service';
import { PositionedGameService } from 'src/app/services/game/positioned-game.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less']
})
export class GameComponent implements OnInit, OnDestroy {
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    if (this.game && !this.game.isOfflineGame) {
      this.gameService.exitGame();
    }
  }

  public game:Game;
  public character: Character;
  public gameReady: boolean = false;
  public isOfflineGame: boolean = false;

  constructor(public gameService: LiveGameService, public offlineGameService: PositionedGameService, public gameDataService: GameDataService, public controlsService:ControlsService) { }

  ngOnInit() {
    if (this.game && !this.game.isOfflineGame) {
      window.onbeforeunload = this.gameService.exitGame;
    }
  }

  ngOnDestroy(): void {
    this.gameService.exitGame();
  }

  public onReady(data:{game:Game, character:Character}) {
    this.game = data.game;
    this.character = data.character;
    this.gameReady = true;
    if(!this.game.isOfflineGame) {
      this.gameService.initBabylon(document.getElementById('renderCanvas'), this.game, this.character);
    } else {
      this.offlineGameService.initBabylon(document.getElementById('renderCanvas'), this.game, this.character);
    }
  }
}

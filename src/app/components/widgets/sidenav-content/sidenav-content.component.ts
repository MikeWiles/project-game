import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav-content',
  templateUrl: './sidenav-content.component.html',
  styleUrls: ['./sidenav-content.component.less']
})
export class SidenavContentComponent implements OnInit {

  constructor(private _router:Router) { }

  ngOnInit() {
  }

  navToPage(page:string) {
    this._router.navigate([page]);
  }
}

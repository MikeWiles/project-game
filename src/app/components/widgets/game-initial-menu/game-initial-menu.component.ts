import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Character } from 'src/app/models/Character';
import { Game } from 'src/app/models/Game';
import { GameDataService } from 'src/app/services/data/game-data.service';

@Component({
  selector: 'app-game-initial-menu',
  templateUrl: './game-initial-menu.component.html',
  styleUrls: ['./game-initial-menu.component.less']
})
export class GameInitialMenuComponent implements OnInit {

  public character: Character = {name:null, colour:null, gameplay: {x: 0, y: 1, z: 0, rotationY: 0, speed: 0, acceleration: 0, state: 'rest'}, animation: { animatables: {} }};
  public game: Game = {characters:[]};
  @Output() triggerReady: EventEmitter<any> = new EventEmitter();
  public games: Game[];

  constructor(private gameDataService: GameDataService) { }

  ngOnInit(): void { 
    this.initAvailableGames();
    // this.character. name = 'mike';
    // this.character.colour = '#33EE33';
    // this.offlineGame();
  }

  initAvailableGames() {
    this.gameDataService.getGames()
    .subscribe(result => {
      this.games = result.map(actions => {
        const _id = actions.payload.doc.id;
        const data = actions.payload.doc.data();
        return {_id,...(<any>data)};
      });
    });
  }

  async offlineGame() {
    if(!this.game.characters) {
      this.game.characters = [];
    }
    this.game._id = 'offline';
    this.character._id = 'player';
    this.game.isOfflineGame = true;
    this.triggerReady.emit({game:this.game, character: this.character});
  }

  async createGame() {
    if(!this.game.characters) {
      this.game.characters = [];
    }
    this.game._id = (await this.gameDataService.createGame(this.game)).id;
    if(this.game._id) {
      this.character._id = (await this.gameDataService.createCharacter(this.game._id,this.character)).id;
    }
    this.triggerReady.emit({game:this.game, character: this.character});
  }

  async joinGame() {
    if(this.game._id) {
      this.character._id = (await this.gameDataService.createCharacter(this.game._id,this.character)).id;
    }
    const subcriptionCharacters = this.gameDataService.getCharacters(this.game._id).subscribe(
      result => {
        this.game.characters = result.map(actions=> {
          const _id = actions.payload.doc.id;
          const data = actions.payload.doc.data();
          return {_id,...(<any>data)};
        });
        this.triggerReady.emit({game:this.game, character: this.character});
        subcriptionCharacters.unsubscribe();
      }
    );
  }
}

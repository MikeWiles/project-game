import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameInitialMenuComponent } from './game-initial-menu.component';

describe('GameInitialMenuComponent', () => {
  let component: GameInitialMenuComponent;
  let fixture: ComponentFixture<GameInitialMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameInitialMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameInitialMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

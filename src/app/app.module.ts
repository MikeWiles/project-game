import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { CharactersComponent } from './components/pages/characters/characters.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from './services/data/user-data.service';
import { SideNavService } from './services/app/side-nav.service';
import { SidenavContentComponent } from './components/widgets/sidenav-content/sidenav-content.component';
import { GameComponent } from './components/pages/game/game.component';
import { AngularResizedEventModule } from 'angular-resize-event';
import { GameInitialMenuComponent } from './components/widgets/game-initial-menu/game-initial-menu.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { GameDataService } from './services/data/game-data.service';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import {MatListModule} from '@angular/material/list';
import { LiveGameService } from './services/game/live-game.service';
import { ControlsService } from './services/game/controls.service';
import { CoreGameService } from './services/game/core-game.service';
import { WorldGameService } from './services/game/world-game.service';
import { PositionedGameService } from './services/game/positioned-game.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CharactersComponent,
    SidenavContentComponent,
    GameComponent,
    GameInitialMenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatTooltipModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireMessagingModule,
    AngularResizedEventModule,
    ColorPickerModule
  ],
  providers: [
    UserService,
    SideNavService,
    CoreGameService,
    WorldGameService,
    GameDataService,
    LiveGameService,
    PositionedGameService,
    ControlsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from "@angular/core";
@Injectable()
export class SideNavService { 

  public _navOpen: boolean = true;

    constructor(){}

    public get navOpen() {
      return this._navOpen;
    }

    public toggleNav() {
      this._navOpen = !this._navOpen;
    }
}
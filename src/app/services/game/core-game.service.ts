import { Injectable, Input } from "@angular/core";
import * as BABYLON from "babylonjs";
import * as BABYLON_GUI from "babylonjs-gui";
import "babylonjs-loaders";
import { Character } from 'src/app/models/Character';
import { Game } from 'src/app/models/Game';
import { GameDataService } from '../data/game-data.service';


@Injectable()
export class CoreGameService {

  @Input() protected character: Character;
  @Input() protected game: Game;

  protected canvas: any;
  protected engine: BABYLON.Engine;
  protected scene: BABYLON.Scene;
  protected light: BABYLON.Light;
  protected camera: BABYLON.ArcFollowCamera;
  protected ground: BABYLON.Mesh;



  constructor(protected gameDataService: GameDataService) { }

  public async initBabylon(canvas: HTMLElement, game: Game, character: Character) {
    this.game = game;
    this.character = character;
    this.canvas = canvas;
    this.engine = new BABYLON.Engine(this.canvas, true, { preserveDrawingBuffer: true, stencil: true });
    await this.setScene(this.engine, this.canvas);
    this.engine.runRenderLoop(() => {
      if(this.camera && this.scene) {
        this.updateScene();
        this.scene.render();
      }
    });
    // the canvas/window resize event handler
    window.addEventListener("resize", () => {
      this.engine.resize();
    });
  }

  public engineResizeViewPort() {
    if (this.engine) {
      this.engine.resize();
      this.canvas.focus();
    }
  }


  public async setScene(engine: BABYLON.Engine, canvas: HTMLCanvasElement) {
    // This creates a basic Babylon Scene object (non-mesh)
    this.scene = new BABYLON.Scene(engine);
    await this.initPlayers();

    // This creates and positions a free camera (non-mesh)
    this.camera = new BABYLON.ArcFollowCamera("arcCamera", BABYLON.Tools.ToRadians(0), BABYLON.Tools.ToRadians(15), 5, this.character.trackingMesh, this.scene);

    // This targets the camera to scene origin
    this.camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    this.camera.attachControl(canvas, true);
    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    this.light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), this.scene);

    // Default intensity is 1. Let"s dim the light a small amount
    this.light.intensity = 0.7;

    // Our built-in "ground" shape. Params: name, width, depth, subdivs, scene
    this.ground = BABYLON.Mesh.CreateGround("ground1", 100, 100, 1, this.scene);
  }

  public async createPlayerObject(character: Character, alpha: number): Promise<Character> {
    let result = await BABYLON.SceneLoader.ImportMeshAsync(null, "./assets/blender/", "character-dude.babylon", this.scene);
    character.mainMesh = result.meshes[0] as BABYLON.Mesh;
    // Attach skeleton for animations
    character.animation.skeleton = result.skeletons[0];

    let sphereMat = new BABYLON.StandardMaterial(`${character._id}-sphereMat`, this.scene);
    sphereMat.specularColor = BABYLON.Color3.FromHexString(character.colour);
    sphereMat.emissiveColor = BABYLON.Color3.FromHexString(character.colour);
    sphereMat.alpha = alpha;
    character.mainMesh.material = sphereMat;

    character.mainMesh.name = character._id;
    character.mainMesh.position.x = character.gameplay.x;
    character.mainMesh.position.z = character.gameplay.z;
    character.mainMesh.position.y = 0.3;

    // Player text
    character.textMesh = BABYLON.Mesh.CreatePlane(`${character._id}-text-plane`, 2, this.scene);
    character.textMesh.parent = character.mainMesh;
    character.textMesh.position.y = 2.3;
    character.textMesh.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

    // Player camera tracking object
    character.trackingMesh = BABYLON.Mesh.CreatePlane(`${character._id}-camera-plane`, 0, this.scene);
    character.trackingMesh.parent = character.mainMesh;
    character.trackingMesh.position.y = 1.5;
    character.trackingMesh.visibility = 0;
    

    let advancedTexture = BABYLON_GUI.AdvancedDynamicTexture.CreateForMesh(character.textMesh);

    let playerText = BABYLON_GUI.Button.CreateSimpleButton(`${character._id}-title`, character.name);
    playerText.width = 1;
    playerText.height = 0.5;
    playerText.color = 'white';
    playerText.fontSize = 60;
    playerText.thickness = 0;

    advancedTexture.addControl(playerText);

    return character;
}

  public removePlayerObject(character:Character) {
    if(character) {
      if(character.trackingMesh) {
        this.scene.removeMesh(character.trackingMesh);
      }
      if(character.textMesh) {
        this.scene.removeMesh(character.textMesh);
      }
      if(character.mainMesh) {
        this.scene.removeMesh(character.mainMesh);
      }
      this.game.characters.splice(this.game.characters.indexOf(character),1);
    }
  }

  public async initPlayers() {
    if (!this.character.gameplay) {
      this.character.gameplay = { x: 0, y: 1, z: 0, rotationY: 0, speed:0, acceleration: 0, state: 'rest' };
    }
    this.character = await this.createPlayerObject(this.character, 0.8);
    this.character.mainMesh.rotation.y = this.camera.rotation.y;
  }

  public updateScene() { }
}

import { Injectable } from '@angular/core';
import * as BABYLON from "babylonjs";
import Peer from 'peerjs';
import { Subscription } from 'rxjs';
import { GameDataService } from '../data/game-data.service';
import { Game } from 'src/app/models/Game';
import { Character, GamePlay } from 'src/app/models/Character';
import { PositionedGameService } from './positioned-game.service';
import { ControlsService } from './controls.service';

@Injectable()
export class LiveGameService extends PositionedGameService {

    //firebase variables
    private subsciptions: Subscription[] = [];

    //peerJS variables
    private peer: Peer;
    private peerConnections: any = {};

  constructor(protected gameDataService:GameDataService, protected controlsService: ControlsService) {
    super(gameDataService, controlsService);
  }

  public exitGame() {
    this.subsciptions.map(s=>s.unsubscribe);
    if (this.game?._id && this.character?._id) {
      if (!this.game.characters || this.game.characters.length <= 1) {
        this.gameDataService.deleteGame(this.game._id);
      } else if (this.character) {
        this.gameDataService.deleteCharacter(this.game._id, this.character._id);
      }
    }
  }

  public async setScene(engine: BABYLON.Engine, canvas: HTMLCanvasElement) {
    await super.setScene(engine,canvas);
  }

  public async initPlayers() {
    await super.initPlayers();
    this.initLiveData();
  }

  public initLiveData() {
        //firebase character subscription
        const subscriptionCharacters = this.gameDataService.getCharacters(this.game._id).subscribe(
          async result => {
            const update = result.map(actions=> {
              const _id = actions.payload.doc.id;
              const data = actions.payload.doc.data();
              return {
                _id,
                ...(<any>data)
              };
            });
            this.game.characters.filter(c=>c._id!=this.character._id).map(c => {
              if(!update.find(cUpdate => cUpdate._id === c._id)) {
                this.removePlayerObject(c);
              }
            });
            this.game.characters = update;
            this.game.characters.filter(c=>c._id!=this.character._id).map(async c=>{
              if(!c.mainMesh) {
                this.createPlayerObject(c,1).then(playerWithMesh => {
                  c = playerWithMesh;
                  if(this.peer) {
                    this.addPeerConnection({_id:c._id,...c.gameplay});
                  }
                });
              }
            });
          }
        );
        this.subsciptions.push(subscriptionCharacters);
        //player peer object for p2p
        this.peer = new Peer(
          `${this.game._id}-${this.character._id}`,
        {
          config: {'iceServers': [
            { urls: ['stun:stun.l.google.com:19302','stun:stun.1.google.com:19302', 'stun:stun.2.google.com:19302'] }
          ]}
        });
        this.peer.on('open',(id)=>{
          this.initPlayerPositionListeners();
          console.log('My peer ID is '+id);
          this.peer.on('connection',(conn)=>{
            this.peerConnections[conn.peer] = conn;
            conn.on('data',(data:GamePlay)=>{
              let player = this.game.characters.find(c=>c._id==data._id);
              player.gameplay = data;
              if(player && player._id) {
                this.updatePlayerLocation(player);
              } else if(conn && conn.peer) {
                this.peerConnections[conn.peer] = conn;
              }
            });
          });
        });
  }


  public unsubscribeData(game:Game, character: Character) {
    this.subsciptions.map(s=>s.unsubscribe);
    if (game?._id && character?._id) {
      if (!this.game.characters || this.game.characters.length <= 1) {
        this.gameDataService.deleteGame(this.game._id);
      } else if (character) {
        this.gameDataService.deleteCharacter(this.game._id, this.character._id);
      }
    }
  }

  public initPlayerPositionListeners() {
    this.game.characters.filter(c=>c._id!=this.character._id).map(c => this.addPeerConnection({_id:c._id,...c.gameplay}));
  }

  public addPeerConnection(gameplay:GamePlay) {
    if(!this.peerConnections[gameplay._id]) {
      const player = this.game.characters.find(c=>c._id==gameplay._id);
      if(player && player._id) {
        this.updatePlayerLocation(player);
        const conn = this.peer.connect(`${this.game._id}-${player._id}`);
        conn.on('open',()=>{
          console.log('connected to peer');
          conn.send({_id:this.character._id,...this.character.gameplay});
          this.peerConnections[player._id] = conn;
        });
      }
    }
  }

  public sendPositionToGamePlayers(character:Character) {
    if(this.peerConnections) {
      this.game.characters.map(c=>{
        if(this.peerConnections[c._id]?.open){
          this.peerConnections[c._id].send({_id:character._id,...character.gameplay})
        }});
    }
  }

  public updatePlayerLocation(player:Character) {
    if(player) {
      if(!player.mainMesh) {
        this.createPlayerObject(player,1).then(c => {
          player = c;
        });
      }
      this.calcUpdatedPlayerObject(player).then(c => player = c);
    }
  }

  public updateScene() {
    super.updateScene();
    this.updateLivePosition();
  }

  public updateLivePosition() {
    super.updateLivePosition(this.character);
    if (this.game && this.character && this.character.mainMesh) {
      if (this.character.gameplay.x != this.character.mainMesh.position.x ||
        this.character.gameplay.z != this.character.mainMesh.position.z ||
        this.character.gameplay.rotationY != this.character.mainMesh.rotation.y) {
        this.character.gameplay.x = this.character.mainMesh.position.x;
        this.character.gameplay.z = this.character.mainMesh.position.z;
        this.character.gameplay.rotationY = this.character.mainMesh.rotation.y;
        this.sendPositionToGamePlayers(this.character);
      }
    }
  }
}

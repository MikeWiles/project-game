import { Injectable } from '@angular/core';
import { Character } from 'src/app/models/Character';
import * as BABYLON from "babylonjs";
import { ControlsService } from './controls.service';
import { WorldGameService } from './world-game.service';
import { GameDataService } from '../data/game-data.service';

@Injectable()
export class PositionedGameService extends WorldGameService {

  private _maxSpeed: number = 0.15;
  private ACCELERATION: number = 0.005;

  constructor(protected gameDataService:GameDataService,protected controlsService: ControlsService) { 
    super(gameDataService);
  }

  public async setScene(engine: BABYLON.Engine, canvas: HTMLCanvasElement) {
    await super.setScene(engine, canvas);
    this.character.mainMesh.rotation.y = this.camera.rotation.y;
    this.character.animation.animatables['rest'] = this.character.animation.skeleton.beginAnimation("Rest", true);
    this.controlsService.initControls(this.scene);
  }

  public async calcUpdatedPlayerObject(updatedPlayerData:Character): Promise<Character> {
    if(updatedPlayerData && updatedPlayerData.mainMesh) {
      updatedPlayerData.mainMesh.position.x = updatedPlayerData.gameplay.x;
      updatedPlayerData.mainMesh.position.z = updatedPlayerData.gameplay.z;
      updatedPlayerData.mainMesh.rotation.y = updatedPlayerData.gameplay.rotationY;
      if(updatedPlayerData.gameplay.state === 'rest') {
        this.startRest(updatedPlayerData, false);
      } else if(updatedPlayerData.gameplay.state === 'run') {
        await this.startRunning(updatedPlayerData, false);
      } else if(updatedPlayerData.gameplay.state === 'walk') {
        await this.startWalking(updatedPlayerData, false);
      } else if(updatedPlayerData.gameplay.state === 'dance') {
        await this.startDancing(updatedPlayerData, false);
      }
    }
    return updatedPlayerData;
  }

  public updatePlayerPositioning() {
    if(this.character && this.character.mainMesh) {
      this.staticTrigger('dance', () => this.startDancing(this.character, true));
      if(this.controlsService.controls['combo'].value) {
        this._maxSpeed = 0.15;
        this.moveTrigger(() => this.startRunning(this.character, true));
      } else {
        this._maxSpeed = 0.08;
        this.moveTrigger(() => this.startWalking(this.character, true));
      }
      this.turnTrigger();
    }
  }

  public moveTrigger(startMovingAnimation: Function) {
    if (this.controlsService.controls['moveForward'].value || this.controlsService.controls['moveBackward'].value) {
      if (this.controlsService.controls['moveForward'].value && this.character.gameplay.speed < this._maxSpeed) {
        this.character.gameplay.speed += this.ACCELERATION;
      }
      if (this.controlsService.controls['moveBackward'].value && this.character.gameplay.speed > -this._maxSpeed) {
        this.character.gameplay.speed -= this.ACCELERATION;
      }
      startMovingAnimation();
    } else {
      this.character.gameplay.speed = 0;
      this.startRest(this.character, true);
    }
    this.character.mainMesh.position.x -= this.character.gameplay.speed * Math.cos(this.camera.alpha);
    this.character.mainMesh.position.z -= this.character.gameplay.speed * Math.sin(this.camera.alpha);
  }

  public staticTrigger(keyTrigger:string, startStaticAnimation: Function) {
    if (this.controlsService.controls[keyTrigger].value) {
      startStaticAnimation();
    }
  }

  public turnTrigger() {
    if (this.controlsService.controls['moveLeft'].value) {
      this.camera.alpha = BABYLON.Tools.ToRadians(BABYLON.Tools.ToDegrees(this.camera.alpha) + 1.5);
      this.character.mainMesh.rotation.y = this.camera.rotation.y + BABYLON.Tools.ToRadians(180);
    }
    if (this.controlsService.controls['moveRight'].value) {
      this.camera.alpha = BABYLON.Tools.ToRadians(BABYLON.Tools.ToDegrees(this.camera.alpha) - 1.5);
      this.character.mainMesh.rotation.y = this.camera.rotation.y + BABYLON.Tools.ToRadians(180);
    }
  }

  public startRest(character: Character, isCurrPlayer?: boolean) {
    if(!character.animation.animatables['rest']) {
      this.resetCharacterAnimations(character, 'rest');
      character.gameplay.speed = 0;
      character.gameplay.state = 'rest';
      character.animation.animatables['rest'] = character.animation.skeleton.beginAnimation("Rest", true);
    }
    if(isCurrPlayer && this.camera) {
      character.mainMesh.rotation.y = this.camera.rotation.y + BABYLON.Tools.ToRadians(180);
      this.updateLivePosition(this.character);
    }
  }

  public async startWalking(character: Character, isCurrPlayer?: boolean) {
    if(!character.animation.animatables['walk']) {
      this.resetCharacterAnimations(character, 'walk');
      character.gameplay.state = 'walk';
      if(character.gameplay.speed === 0) {
      await character.animation.skeleton.beginAnimation("RestToWalk").waitAsync();
      }
      if(character.gameplay.speed > 0.08) {
        character.gameplay.speed = 0.08;
      }
      character.animation.animatables['walk'] = character.animation.skeleton.beginAnimation("Walk", true);
    }
    if(isCurrPlayer && this.camera) {
      character.mainMesh.rotation.y = this.camera.rotation.y + BABYLON.Tools.ToRadians(180);
      this.updateLivePosition(this.character);
    }
  }

  public async startRunning(character: Character, isCurrPlayer?: boolean) {
    if(!character.animation.animatables['run']) {
      this.resetCharacterAnimations(character, 'run');
      character.gameplay.state = 'run';
      if(character.gameplay.speed === 0) {
        await character.animation.skeleton.beginAnimation("RestToRun").waitAsync();
      }
      character.animation.animatables['run'] = character.animation.skeleton.beginAnimation("Run", true);
    }
    if(isCurrPlayer && this.camera) {
      character.mainMesh.rotation.y = this.camera.rotation.y + BABYLON.Tools.ToRadians(180);
      this.updateLivePosition(this.character);
    }
  }

  public async startDancing(character: Character, isCurrPlayer?: boolean) {
    if(!character.animation.animatables['dance']) {
      this.resetCharacterAnimations(character, 'dance');
      character.gameplay.state = 'dance';
      await character.animation.skeleton.beginAnimation("RestToDance").waitAsync();
      character.animation.animatables['dance'] = await character.animation.skeleton.beginAnimation("Dance").waitAsync();
      this.startRest(character, isCurrPlayer);
    }
    if(isCurrPlayer && this.camera) {
      this.updateLivePosition(character);
    }
  }

  public resetCharacterAnimations(character: Character, animatableKey: string) {
    for(let key in character.animation.animatables) {
      if(!!character.animation.animatables[key] && key != animatableKey) {
        character.animation.animatables[key].stop();
        character.animation.animatables[key] = null;
      }
    }
  }

  public updateScene() {
    super.updateScene();
    this.updatePlayerPositioning();
  }

  public updateLivePosition(character:Character) { }
}

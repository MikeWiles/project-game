import { Injectable } from '@angular/core';
import * as BABYLON from "babylonjs";
import { Key } from 'ts-keycode-enum';
import { KeyControl } from 'src/app/models/KeyControl';

@Injectable()
export class ControlsService {

  private mouseControl = false;
  public controls: {[key:string]:KeyControl} = {
    'moveForward':{
      value:0,
      keys: [Key.UpArrow, Key.W]
    },
    'moveBackward':{
      value:0,
      keys: [Key.DownArrow, Key.S]
    },
    'moveLeft':{
      value:0,
      keys: [Key.LeftArrow, Key.A]
    },
    'moveRight':{
      value:0,
      keys: [Key.RightArrow, Key.D]
    },
    'combo':{
      value:0,
      keys: [Key.Shift, Key.Alt]
    },
    'dance':{
      value:0,
      keys: [Key.Comma]
    }
  };

  constructor() { }

  public initControls(scene: BABYLON.Scene) {
    scene.actionManager = new BABYLON.ActionManager(scene);
    for(let identifier in this.controls) {
      this.controls[identifier].keys.forEach((k:Key)=>{
        scene.actionManager.registerAction(
          new BABYLON.ExecuteCodeAction({
            trigger: BABYLON.ActionManager.OnKeyDownTrigger,
            parameter: k
          },
          (e) => {
            this.controls[identifier].value = 1
          })
        );
        scene.actionManager.registerAction(
          new BABYLON.ExecuteCodeAction({
            trigger: BABYLON.ActionManager.OnKeyUpTrigger,
            parameter: k
          },
            (e) => {
              if(!this.mouseControl) {
                this.controls[identifier].value = 0;
              }
            })
        );
      });
    };
  }
  
  public moveHold(direction:string) {
    this.mouseControl = true;
    switch(direction.toLowerCase()) {
      case "forward":
        this.controls["moveForward"].value = 1;
      break
      case "backward":
      this.controls["moveBackward"].value = 1;
      break
      case "left":
      this.controls["moveLeft"].value = 1;
      break
      case "right":
      this.controls["moveRight"].value = 1;
      break
    }
  }

  public moveRelease(direction:string) {
    this.mouseControl = false;
    switch(direction.toLowerCase()) {
      case "forward":
        this.controls["moveForward"].value = 0;
      break
      case "backward":
      this.controls["moveBackward"].value = 0;
      break
      case "left":
      this.controls["moveLeft"].value = 0;
      break
      case "right":
      this.controls["moveRight"].value = 0;
      break
    }
  }
}

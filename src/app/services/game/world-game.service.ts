import { Injectable, Input } from "@angular/core";
import * as BABYLON from "babylonjs";
import { Character} from 'src/app/models/Character';
import { Game } from 'src/app/models/Game';
import { GameDataService } from '../data/game-data.service';
import { CoreGameService } from './core-game.service';
import { ControlsService } from './controls.service';


@Injectable()
export class WorldGameService extends CoreGameService {

  @Input() character: Character;
  @Input() game: Game;

  constructor(protected gameDataService: GameDataService) 
  { 
    super(gameDataService);
  }

  public async setScene(engine: BABYLON.Engine, canvas: HTMLCanvasElement) {
    await super.setScene(engine,canvas);
    var groundMat = new BABYLON.StandardMaterial("groundMat", this.scene);
    groundMat.diffuseTexture = new BABYLON.Texture("./assets/images/grass.jpg", this.scene);
    groundMat.roughness = 1;
    groundMat.diffuseTexture._texture
    this.ground.material = groundMat;
    this.setUVScale(this.ground, 1000, 1000);
  }

  public async initPlayers() {
    if (!this.character.gameplay) {
      this.character.gameplay = { x: 0, y: 1, z: 0, rotationY: 0, speed:0, acceleration: 0, state: 'rest' };
    }
    this.character = await this.createPlayerObject(this.character, 0.75);
  }

  public setUVScale(mesh, uScale, vScale) {
		var i,
			UVs = mesh.getVerticesData(BABYLON.VertexBuffer.UVKind),
			len = UVs.length;
		
		if (uScale !== 1) {
			for (i = 0; i < len; i += 2) {
				UVs[i] *= uScale;
			}
		}
		if (vScale !== 1) {
			for (i = 1; i < len; i += 2) {
				UVs[i] *= vScale;
			}
		}
		
		mesh.setVerticesData(BABYLON.VertexBuffer.UVKind, UVs);
	}
}
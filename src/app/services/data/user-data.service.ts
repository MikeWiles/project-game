import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { User } from '../../models/User';
import { Injectable } from "@angular/core";


@Injectable()
export class UserService { 

    constructor(public db: AngularFirestore){}

    //create single
    public createUser(user:User):Promise<DocumentReference>{
        return this.db.collection('users').add(user);
    }

    //get single
    public async getUser(id:string){
        return this.db.collection('users').doc(id).snapshotChanges();
      }

    //get many
    getUsers(){
        return this.db.collection('users').snapshotChanges();
      }

    //update single
    updateUser(userKey, value){
    value.nameToSearch = value.name.toLowerCase();
    return this.db.collection('/users').doc(userKey).set(value);
    }

    //delete single
    deleteUser(userKey){
        return this.db.collection('users').doc(userKey).delete();
      }
}
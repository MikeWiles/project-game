import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Game } from '../../models/Game';
import { Injectable } from "@angular/core";
import { Character } from 'src/app/models/Character';


@Injectable()
export class GameDataService { 

    constructor(public db: AngularFirestore){}

    //create single
    public createGame(game:Game):Promise<DocumentReference>{
        return this.db.collection('games').add(game);
    }

    //get single
    public getGame(id:string){
        return this.db.collection('games').doc(id).valueChanges();
      }

    //get many
    getGames(){
      return this.db.collection('games').snapshotChanges();
    }

    //update single
    updateGame(gameKey, value){
    value.nameToSearch = value.name.toLowerCase();
    return this.db.collection('games').doc(gameKey).set(value);
    }

    //delete single
    deleteGame(gameKey){
      return this.db.collection('games').doc(gameKey).delete();
    }

    //get single
    public getCharacter(gameKey,characterKey:string){
      return this.db.collection('games').doc(gameKey).collection('characters').doc(characterKey).valueChanges();
    }

    //get many
    getCharacters(gameKey){
      return this.db.collection('games').doc(gameKey).collection('characters').snapshotChanges();
    }

    //create single
    public createCharacter(gameKey, character:Character):Promise<DocumentReference>{
      return this.db.collection('games').doc(gameKey).collection('characters').add(character);
    }

    //update single inside
    updateCharacter(gameKey, characterKey, value){
      value.nameToSearch = value.name.toLowerCase();
      return this.db.collection('games').doc(gameKey).collection('characters').doc(characterKey).set(value);
    }

    //delete single
    deleteCharacter(gameKey, characterKey){
      return this.db.collection('games').doc(gameKey).collection('characters').doc(characterKey).delete();
    }
}